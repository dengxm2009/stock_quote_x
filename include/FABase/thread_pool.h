#pragma once
#include "AutoPt.h"
#include "thread.h"
#include "safe_vector.h"

enum threadpool_state_t 
{
	UNINITIALIZED, 
	INITIALIZED,
};

#define DEFAULT_THREADS_NUM  10 

class CYKThreadPool;

typedef std::vector<CYKAutoThread>::const_iterator citr_type;

class CYKThreadGroup
{
public:
	CYKThreadGroup();
	~CYKThreadGroup();
	bool addThread(CYKAutoThread thread);
	bool join();
	size_t size();
	bool terminateAll();
private:
	CYKThreadGroup(const CYKThreadGroup&);
	CYKThreadGroup& operator=(const CYKThreadGroup&);
private:
	std::vector<CYKAutoThread> _threads;
};

class CYKThreadPoolRunner :public IRunnable
{
public:
	CYKThreadPoolRunner(CYKThreadPool* tp);
	virtual ~CYKThreadPoolRunner(){}
	virtual int32_t operator()(const bool* isstopped, void* param);
private:
	CYKThreadPool* _tp;
};

typedef CYKAutoPoint<CYKThreadPoolRunner> CYKAutoThreadPoolRunner;

class CYKThreadPool:public CYKCounter
{
public:
	CYKThreadPool();
	~CYKThreadPool();

	int init(int nThreads = DEFAULT_THREADS_NUM);	//初始化线程池，创建规定个数的线程
	bool addTask(RunnableAutoPt &pr);				//向任务队列中添加任务
	bool join();									//等待所有线程执行完毕。请不要调用此方法，线程池中的线程永远不会自动执行结束
	size_t size();									//当前线程池中线程个数
	bool terminate();								//终止所有线程的执行
	CYKSafeVector<RunnableAutoPt>&	getTask(){ return _tasks; }
private:
	int addWorker(int nWorker);						//向线程池中添加nWorker个线程
private:
	CYKSafeVector<RunnableAutoPt> _tasks;	//添加到线程池中的任务
	CYKThreadGroup _threadGroup;			//线程组
	threadpool_state_t _state;

};