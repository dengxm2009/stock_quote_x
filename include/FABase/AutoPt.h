#pragma once

/************************************************************************/
/* 
		智能指针
*/
/************************************************************************/

#include "lock.h"

class CYKCounter
{
public:
	CYKCounter()
		:m_counter(0)
	{
	}
	virtual ~CYKCounter()
	{

	}
private:
	CYKCounter(const CYKCounter &pt);
	CYKCounter &operator=(const CYKCounter &pt);

public:
	void inc(){

		CYKAutoMutex _mutex(&m_mutex);
		++m_counter;
	}
	int32_t dec()
	{
		CYKAutoMutex _mutex(&m_mutex);
		return --m_counter;
	}
	int32_t getCounter()
	{
		CYKAutoMutex _mutex(&m_mutex);
		return m_counter;
	}
	void free()
	{
		delete this;
	}

private:
	CYKMutex m_mutex;
	volatile int32_t m_counter;
};

//通过内存池申请的指针
class CYKMemCounter
{
public:
	CYKMemCounter()
		:m_counter(0)
	{
	}
	virtual ~CYKMemCounter()
	{

	}
private:
	CYKMemCounter(const CYKCounter &pt);
	CYKMemCounter &operator=(const CYKMemCounter &pt);

public:
	void inc(){

		CYKAutoMutex _mutex(&m_mutex);
		++m_counter;
	}
	int32_t dec()
	{
		CYKAutoMutex _mutex(&m_mutex);
		return --m_counter;
	}
	int32_t getCounter()
	{
		CYKAutoMutex _mutex(&m_mutex);
		return m_counter;
	}
	void free();


private:
	CYKMutex m_mutex;
	volatile int32_t m_counter;
};



template <typename T>
class CYKAutoPoint
{
public:
	explicit CYKAutoPoint(T *pt)
		:m_pt(pt)
	{
		if (m_pt)
			m_pt->inc();
	}
	~CYKAutoPoint(){
		if (m_pt && m_pt->dec() == 0)
		{
			//delete m_pt;
			//m_pt = 0;
			m_pt->free();
		}
	}
	CYKAutoPoint &operator=(const CYKAutoPoint &pt)
	{
		if (m_pt != pt.m_pt)
		{
			if (NULL !=pt.get())
			{
				pt.get()->inc();
			}
		}

		T * ptt = m_pt;
		m_pt = pt.get();
		if (NULL != ptt)
		{
			if (ptt->dec()==0)
			{
				//delete ptt;
				//ptt = 0;

				ptt->free();
			}
		}
		return *this;
	}

	T* operator->() const
	{
		return m_pt;
	}

	T &operator*()
	{
		return *m_pt;
	}

	CYKAutoPoint(const CYKAutoPoint &pt)
		:m_pt(pt.m_pt)
	{
		if (m_pt)
		{
			m_pt->inc();
		}
	}

	template <typename U>
	CYKAutoPoint(const CYKAutoPoint<U> &pt)
		:m_pt(pt.get())
	{
		if (m_pt)
		{
			m_pt->inc();
		}
	}


	T * get() const
	{
		return m_pt;
	}

	bool isValid()
	{
		return m_pt ? true : false;
	}

private:
	T * m_pt;
};
//
//typedef CYKAutoPoint<CYKCounter> CAutoPt;
//typedef CYKAutoPoint<CYKMemCounter> CAutoMemPt;