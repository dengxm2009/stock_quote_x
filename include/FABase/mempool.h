﻿#pragma once

/************************************************************************/
/* 
	内存池，分配的内存赋值给智能指针，释放后归返内存池
*/
/************************************************************************/

#include "common.h"
#include "Lock.h"
#include <map>
#include "AutoPt.h"

//支持的chunk大小（对原始size进行adjust）/其他更大内存数据，malloc直接申请
#define CHUNK_128	128
#define CHUNK_256	256
#define CHUNK_512	512
#define CHUNK_1024	1024
#define CHUNK_2048	2048
#define CHUNK_MAX	4096

typedef  void* LPChunk;
typedef  void** LPPChunk;

typedef std::vector<LPChunk> VChunk;

class CMemPool
{
public:
	/*初始化创建较大的内存区域*/
	CMemPool();
	~CMemPool();
public:
	//调整大小
	int32_t Adjust(int32_t chunk_size);
	//释放全部
	void	Clear(int32_t chunk_size);
	//申请一个内存
	LPChunk	AllocChunk(int32_t size);
	//释放一个内存
	void	FreeChunk(LPPChunk pt);
public:
	static CMemPool * Instance();

private:
	CMemPool(const CMemPool&);
	CMemPool&operator=(const CMemPool&);

private:
	//分配的block地址
	std::map<int32_t, VChunk>	mBlocks;	
	CYKMutex		mMutex;
};

#define MEMPOOL CMemPool::Instance()