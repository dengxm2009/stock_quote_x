#pragma once
#include "time.h"

// 日期和时间
struct tagYKTime
{
	int nYear;
	int nMonth;
	int nDay;
	int nHour;
	int nMinute;
	int nSecond;
	int nMilliseconds;
	int nDayOfWeek;
};

class CTools
{
public:
	//time
	static time_t GetTime();
	static void GetTime(tm&tmtemp);

	//math
	static bool IsDigital(char c);		//0-9
	static bool IsCharacter(char c);//a-z

protected:
private:
};