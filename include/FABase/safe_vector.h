#pragma once
/************************************************************************/
/* 
	线程安全的vector
*/
/************************************************************************/

#include "Lock.h"
template <typename T>
class CYKSafeVector:public CYKCounter
{
public:
	CYKSafeVector()
	{
		_psem = new CYKSemaphore();
	}
	~CYKSafeVector()
	{
		SAFE_DEL(_psem);
	}

	int32_t put(T &obj) {
		CYKAutoMutex mutex(&_mutex);
		if (mutex.isLocked()) {
			try {
				_queue.push(obj);
			}
			catch (std::bad_alloc &) {
				return E_NOMEM;
			}
		}
		else {
			return E_SYSERROR;
		}		
		_psem->signal();
		return 0;
	}

	int32_t get(T& obj, int millisecond) {
		_rwlock_clear.get_rdlock();
		int32_t ret = _psem->wait(millisecond);

		if (0 == ret) {	
			{
				CYKAutoMutex mutex(&_mutex);
				if (mutex.isLocked()) {
					obj = _queue.front();
					_queue.pop();
				}
				else {
					_rwlock_clear.unlock();
					return E_SYSERROR;
				}
			}
			_rwlock_clear.unlock();
			return 0;
		}
		else {
			_rwlock_clear.unlock();
			return ret;
		}
	}

	int32_t size() {
		return (int32_t)_queue.size();
	}

	void clear() {
		_rwlock_clear.get_wrlock();
		CYKAutoMutex mutex(&_mutex);
		while (_queue.size()){
			_queue.pop();
		}
		delete _psem;
		_psem = new Semaphore();
		_rwlock_clear.unlock();
	}

private:
	CYKSafeVector(const CYKSafeVector&);
	CYKSafeVector&operator=(const CYKSafeVector&);

private:
	std::queue<T> _queue;
	CYKMutex _mutex;
	CYKRWLock _rwlock_clear;
	CYKSemaphore * _psem;
};
