
#pragma once

#ifdef WIN32

#include <windows.h>

struct CAtom
{
public:
	CAtom()
	{
		m_l = 0;
	}

	volatile long* Get()
	{
		return &m_l;
	}
private:
	volatile long m_l;
};

//ԭ����
//�м�Ƕ��ʹ��
class CLockAtom
{
public:
	CLockAtom(void)
	{
		m_ics = 0;
	}
	
	~CLockAtom(void){}

	inline void Lock()
	{
		while (InterlockedExchange(&m_ics, 1) == 1)
		{
			Sleep(0);
		}
	}
	
	inline void UnLock()
	{
		InterlockedExchange(&m_ics, 0);
	}

	static inline void Lock(CAtom* pAtom)
	{
		while (InterlockedExchange(pAtom->Get(), 1) == 1)
		{
			Sleep(0);
		}
	}

	static inline void UnLock(CAtom* pAtom)
	{
		InterlockedExchange(pAtom->Get(), 0);
	}
	
private:
	volatile long m_ics;
};

class CLockAtomRegion
{
public:
	CLockAtomRegion(CLockAtom* lock)
	{
		m_atom = NULL;
		m_lock = lock;
		m_lock->Lock();
	}

	CLockAtomRegion(CAtom* atom)
	{
		m_atom = atom;
		m_lock = NULL;
		CLockAtom::Lock(m_atom);
	}

	~CLockAtomRegion()
	{
		if (m_lock)
		{
			m_lock->UnLock();
		}
		else if (m_atom)
		{
			CLockAtom::UnLock(m_atom);
		}
	}
private:
	CLockAtom* m_lock;
	CAtom* m_atom;
};

#endif