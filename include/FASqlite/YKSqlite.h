#pragma once

#include <stdio.h>
#include "ISqlite.h"
#include "dllHelper.h"

class sqlite3;

class CYKSqlite:public ISqlite
{
public:
	CYKSqlite(void);
	~CYKSqlite(void);

	virtual bool	Open(char* lpszFileName,int nFlag);
	virtual bool	IsOpen();
	virtual bool	Close();
	virtual bool	Execute(char* lpszSQL, YKSQLITE_CALLBACK pCallback, void *pUnuse, int &nErrCode);
	virtual char*	GetLastError();
	virtual int		GetLastErrorCode(){return m_nLastErrorCode;}
	
protected:
	sqlite3  *m_pDatabase;
	char     *m_pLastError;
	int       m_nLastErrorCode;

};

extern "C"
{
	FA_API ISqlite* CreateObject(ISqliteHandle *p);
	FA_API void ReleaseObject();
}


extern CYKSqlite * gYKSqlite;