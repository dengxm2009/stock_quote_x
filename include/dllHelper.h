#pragma once
#include <stdio.h> 
#include <string>
#include "FABase/AutoPt.h"
#ifdef WIN32
#include "windows.h"

	#ifdef FA_EXPORTS
	#define FA_API __declspec(dllexport)
	#else
	#define FA_API __declspec(dllimport)
	#endif

#else
#endif


// 动态库辅助类
class CDllHelper:public CYKCounter
{
protected:
	char					m_szFileName[_MAX_DIR];                  // 文件名
    HMODULE					m_hLibrary;                    // 模块句柄

public:
	CDllHelper(const char *lpszFileName) :
		m_hLibrary(NULL)
	{
		sprintf_s(m_szFileName, _MAX_DIR, lpszFileName);
		Load();
	}
	~CDllHelper(){
		if (m_hLibrary != NULL)
		{
			::FreeLibrary(m_hLibrary);
			m_hLibrary = NULL;
		}
	}

public:
    // 获取Proc
	template<typename Proc>
	Proc GetProcedure(LPCSTR lpszProc)
	{
		Load();

        Proc pfnProc = NULL;
		if (m_hLibrary != NULL) pfnProc = (Proc)::GetProcAddress(m_hLibrary, lpszProc);

		return pfnProc;
	}

    // 加载动态库
	void Load()
	{
		if (m_hLibrary != NULL) return;

		char szPath[MAX_PATH] = { 0 };

		HMODULE hModule = ::GetModuleHandle(NULL);
		if (hModule != NULL)
		{
			DWORD dwLen = GetModuleFileNameA(hModule, szPath, MAX_PATH);
			if (dwLen != 0)
			{
				char szDrive[_MAX_DRIVE];
				char szDirectory[_MAX_DIR];

				_splitpath_s(szPath, szDrive, _MAX_DRIVE, szDirectory, _MAX_DIR, NULL, 0, NULL, 0);
				_makepath_s(szPath, MAX_PATH, szDrive, szDirectory, NULL, NULL);

				strcat_s(szPath, MAX_PATH, m_szFileName);
				m_hLibrary = ::LoadLibraryA(szPath);
				if(!m_hLibrary)
				{
					printf("[Err][%s][%s]%d\n", m_szFileName,__FUNCTION__, GetLastError());
				}
			}
		}
		
	}
    // 是否已经加载动态库
	bool IsLoaded()
	{
		return m_hLibrary != NULL;
	}
};


typedef CYKAutoPoint<CDllHelper> CDllHelperAuto;