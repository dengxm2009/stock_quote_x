#ifndef __UILABEL_H__
#define __UILABEL_H__

#pragma once

namespace DuiLib
{
	class UILIB_API CLabelUI : public CControlUI
	{
	public:
		CLabelUI();

		LPCTSTR GetClass() const;
		LPVOID GetInterface(LPCTSTR pstrName);

        virtual void SetText(LPCTSTR pstrText);
        virtual void SetFloat(bool bFloat = true);
		void SetTextStyle(UINT uStyle);
		UINT GetTextStyle() const;
		void SetTextColor(DWORD dwTextColor);
		DWORD GetTextColor() const;
		void SetDisabledTextColor(DWORD dwTextColor);
        DWORD GetDisabledTextColor() const;
        void SetTextExColor(DWORD dwTextColor) { m_dwTextExColor = dwTextColor;}
        DWORD GetTextExColor() const    { return m_dwTextExColor;}
		void SetFont(int index);
		int GetFont() const;
		RECT GetTextPadding() const;
		void SetTextPadding(RECT rc);
		bool IsShowHtml();
		void SetShowHtml(bool bShowHtml = true);
        bool IsAutoSize()const { return m_bAutoSize;}
        bool IsMultiLine()const { return m_bMultiLine;}
        bool IsTextNoClip()const { return (m_uTextStyle & DT_NOCLIP) != 0;}
        bool IsTextShadow()const { return (m_uTextStyle & DT_SHADOW) != 0;}
        void SetAutoSize(bool bAutoSize = true);
        void SetMultiLine(bool bMultiLine = true);
        void SetTextNoClip(bool bClip = true) { if(bClip){ if((m_uTextStyle & DT_NOCLIP) == 0) m_uTextStyle |= DT_NOCLIP;} else { if((m_uTextStyle & DT_NOCLIP) != 0) m_uTextStyle = m_uTextStyle & ~DT_NOCLIP;}}
        void SetShowShadow(bool bShadow = true) { if(bShadow){ if((m_uTextStyle & DT_SHADOW) == 0) m_uTextStyle |= DT_SHADOW;} else { if((m_uTextStyle & DT_SHADOW) != 0) m_uTextStyle = m_uTextStyle & ~DT_SHADOW;}}

		SIZE EstimateSize(SIZE szAvailable);
        SIZE CalcTextSize(HDC hdc,LPCTSTR txt);
		virtual void DoEvent(TEventUI& event);
		void SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);

        void PaintText(HDC hDC);
        void Invalidate();

        const CDuiString& GetLinkContent(int iIndex) { return m_sLinks[iIndex]; }
	protected:
		DWORD m_dwTextColor;
		DWORD m_dwDisabledTextColor;
        DWORD m_dwTextExColor;  // ��չ��ɫ
		int m_iFont;
		UINT m_uTextStyle;
		RECT m_rcTextPadding;
		bool m_bShowHtml;
        bool m_bAutoSize;
        bool m_bMultiLine;
        // ������
        enum { MAX_LINK = 8 };
        int m_nLinks;
        RECT m_rcLinks[MAX_LINK];
        CDuiString m_sLinks[MAX_LINK];
        int m_nHoverLink;
	};
}

#endif // __UILABEL_H__