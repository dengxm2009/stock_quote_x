#ifndef __UIMENU_H__
#define __UIMENU_H__

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//										 CMenuUI  菜单控件
#include "vector"
using namespace std;



namespace DuiLib{

    class CMenuWnd;
    struct MenuNode
    {
        HWND m_thishWnd;
        struct MenuNode *nextMenu;
    };
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //										 CMenuListElementUI  窗体
    class UILIB_API CMenuListElementUI : public CListElementUI
    {
    public:
        virtual LPCTSTR GetClass() const;
        virtual LPVOID GetInterface(LPCTSTR pstrName);
        void SetButtonState(UINT state);
    };

    class UILIB_API CMenuUI:
        public CVerticalLayoutUI,
        public IListOwnerUI
    {
        friend CMenuWnd;							//友元类
    public:
        CMenuUI();

        virtual LPCTSTR GetClass() const;					//获得类名
        virtual LPVOID GetInterface(LPCTSTR pstrName);		//获得接口
        void initMenuLayout();


        UINT GetControlFlags() const;				//控件标志

        CDuiString GetText() const;					//控件内容
        void SetEnabled(bool bEnable = true);		//设置活动与否

        CDuiString GetDropBoxAttributeList();			//得到拖动属性列表
        void SetDropBoxAttributeList(LPCTSTR pstrList);	//设置拖动属性列表
        SIZE GetDropBoxSize() const;					//得到拖动箱子大小
        void SetDropBoxSize(SIZE szDropBox);			//设置拖动箱子的大小

        int GetCurSel() const;							//得到鼠标选择项
        bool SelectItem(int iIndex, bool bTakeFocus = false);					//选择项

        bool SetItemIndex(CControlUI* pControl, int iIndex);	//设置选择项的序号
        bool Add(CControlUI* pControl);							//添加控件
        bool AddAt(CControlUI* pControl, int iIndex);			//控件定位
        bool Remove(CControlUI* pControl);						//移除
        bool RemoveAt(int iIndex);								//移除指定位置
        void RemoveAll();										//移除全部

        bool Activate(POINT pt);										//激活主菜单
        bool ActivateNextMenu(POINT pt,HWND hWndPare,RECT eventrc);								//根据上次菜单的数据 激活下级菜单

        TListInfoUI* GetListInfo();								//列表信息
        void SetItemFont(int index);							//设置节点字体
        void SetItemTextStyle(UINT uStyle);						//设置节点文本类型
        void SetItemTextPadding(RECT rc);						//设置节点文本位置
        void SetItemTextColor(DWORD dwTextColor);				//设置节点文本的颜色
        void SetItemBkColor(DWORD dwBkColor);					//设置节点的背景颜色
        void SetItemBkImage(LPCTSTR pStrImage);
        LPCTSTR GetItemBkImage();
        void SetMenuWidth(LPCTSTR pValue);
        int GetMenuWidth();
        SIZE GetMenuCorner();
        void SetMenuCorner(int cx, int cy);
        //void SetItemImage(LPCTSTR pStrImage);					//设置节点的图片
        void SetSelectedItemTextColor(DWORD dwTextColor);		//设置选中节点文本背景颜色
        void SetSelectedItemBkColor(DWORD dwBkColor);			//设置选中节点背景颜色
        void SetSelectedItemImage(LPCTSTR pStrImage);			//设置选中节点图片
        void SetHotItemTextColor(DWORD dwTextColor);			//设置最新节点文本颜色
        void SetHotItemBkColor(DWORD dwBkColor);				//设置最新。。。
        void SetHotItemImage(LPCTSTR pStrImage);
        void SetDisabledItemTextColor(DWORD dwTextColor);
        void SetDisabledItemBkColor(DWORD dwBkColor);
        void SetDisabledItemImage(LPCTSTR pStrImage);
        void SetItemLineColor(DWORD dwLineColor);				//设置节点线的颜色
        bool IsItemShowHtml();									//判断是否显示html
        void SetItemShowHtml(bool bShowHtml = true);			//设置节点显示

        SIZE EstimateSize(SIZE szAvailable);					//估计大小
        void SetPos(RECT rc);									//设置大小区域
        virtual void OnEvent(TEventUI& event) { DoEvent(event); }
        virtual void DoEvent(TEventUI& event);							//定义事件
        void SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);	//设置属性

        void DestroyMenu();
        void EnableMenuItem(LPCTSTR pstrName,BOOL pValue);						//使选择项恢复
        //void EnableNextMenuItem(int labelId,BOOL pValue, BOOL& bHandled);
        BOOL GetItemEnabelState(LPCTSTR pstrName);
        BOOL GetItemNextEnableState(LPCTSTR pstrName,BOOL& ItemState, BOOL& bHandled);
        void CheckMenuItem(LPCTSTR pstrName,BOOL pValue);
        BOOL GetItemNextCheckState(LPCTSTR pstrName,BOOL& ItemState, BOOL& bHandled);
        BOOL GetItemCheckState(LPCTSTR pstrName);
        //int GetMenuItemId(LPCTSTR pstrName);
        //void UnEnableMenuItem(int index);					//选择项不可用
        //void SetMarkCheckOk(BOOL pValue,int index);			//设置复选框  读取文件设置复选框标识
        //void SetMarkCheckCancel(BOOL pValue,int index);
        void DoPaint(HDC hDC, const RECT& rcPaint);				//控件绘画

        virtual bool IsFocusAble()const { return true;}
    protected:
        void        calcSelectIndex();

    public:
        CMenuWnd* m_pWindow;			//定义一个窗口对象
        CVerticalLayoutUI* m_pMenuLayout;
        int MenuWidth;
        SIZE RoundCorner;
    public:
        HWND hWndParent;				//主菜单的句柄
        static vector<HWND> hWndList;	
        int m_iCurSel;					//当前选择的序号
        RECT m_rcTextPadding;			//文本的位置区域
        CDuiString m_sDropBoxAttributes;	//字符串 拖动箱子属性
        CDuiSize m_szDropBox;					//拖动箱子大小

        TListInfoUI m_ListInfo;				//列表框的信息		TListInfoUI为结构
    };



    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //										 CMenuWnd   窗体
    class CMenuWnd:
        public CWindowWnd
    {
    public:
        CMenuWnd();
        void Init(CMenuUI* pOwner,POINT pt);
        void InitNextMenu(CMenuUI* pOwner,POINT pt,HWND hWndPare,RECT eventrc);
        LPCTSTR GetWindowClassName() const;
        HWND GetMainWindowHwnd();
        void OnFinalMessage(HWND hWnd);
        LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);

    private:
        CPaintManagerUI m_pm;
        CMenuUI* m_pOwner;
        CMenuUI* m_pEventOwner;
    public:
        CVerticalLayoutUI* m_pLayout;
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //										 CMenuLabelElementUI  窗体
    class UILIB_API CMenuLabelElementUI:
        public CMenuListElementUI
    {
    public:
        CMenuLabelElementUI();

        virtual LPCTSTR GetClass() const;
        virtual LPVOID GetInterface(LPCTSTR pstrName);

        virtual void DoEvent(TEventUI& event);
        SIZE EstimateSize(SIZE szAvailable);
        void DoPaint(HDC hDC, const RECT& rcPaint);
        void DrawItemText(HDC hDC, const RECT& rcItem);
        void PaintMarkImage(HDC hDC);
        void PaintBackMarkImage(HDC hDC);
        void SetNextMenu(LPCTSTR pstrValue);
        CDuiString GetNextMenu();
        CDuiString GetLabelType();
        void SetLabelType(LPCTSTR pstrValue);
        void SetNextMenuMark(LPCTSTR pstrValue);
        CDuiString GetNextMenuMark();
        CDuiString GetLabelMark();
        void SetLabelMark(LPCTSTR pstrValue);
        void SetLabelMarkCheck(BOOL pstrValue);
        BOOL GetLabelMarkCheck();
        void SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);	//设置属性

        virtual void UpdateAnimation(CPaintManagerUI*m);
    private:
        CDuiString LabelType;
        CDuiString NextMenu;			//下级菜单   子菜单
        TAnimation LabelMark;			// 标签图标
        BOOL LabelMarkCheck;			//设置复选 判断
        TAnimation NextMenuMark;		//下级菜单图标  标志
    };

};
#endif // __UIMENU_H__
