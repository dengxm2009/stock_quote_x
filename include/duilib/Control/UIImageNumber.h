#ifndef __UIIMAGENUMBER_H__
#define __UIIMAGENUMBER_H__

#pragma once

namespace DuiLib
{
	class UILIB_API CImageNumberUI : public CControlUI
	{
	public:
		CImageNumberUI();
		~CImageNumberUI();

		LPCTSTR GetClass() const;
		UINT GetControlFlags() const;
		LPVOID GetInterface(LPCTSTR pstrName);

		LPCTSTR GetNumberImage();
		void    SetNumberImage(LPCTSTR pStrImage);

		void    SetNumber(__int64 n)    { m_nNumber = n;Invalidate();}
		__int64 GetNumber()const    { return m_nNumber;}

		void	SetImageFrameCount(int n) { m_nFrameCount = n;}
		int	    GetImageFrameCount()const { return m_nFrameCount;}

		void	SetSymbolPlus(int n)	{ m_nSymbolPlusIdx = n;}
		void	SetSymbolNeg(int n)		{ m_nSymbolNegIdx = n;}
		void	SetSymbolMoney(int n)	{ m_nSymbolMoneyIdx = n;}
		int	    GetSymbolPlus()const  { return m_nSymbolPlusIdx;}
		int	    GetSymbolNeg()const   { return m_nSymbolNegIdx;}
		int	    GetSymbolMoney()const { return m_nSymbolMoneyIdx;}

        DWORD   GetAlignStyle()const    { return m_uAlignStyle;}
        void    SetAlignStyle(DWORD d)  { m_uAlignStyle = d;}

		void PaintText(HDC hDC);
		void SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);

	protected:
		__int64     m_nNumber;
		int         m_nFrameCount;
		int         m_nWordSpace;
		int         m_nSymbolPlusIdx;
		int         m_nSymbolNegIdx;
		int         m_nSymbolMoneyIdx;
		DWORD       m_uAlignStyle;
		TAnimation  m_sNumberImage;
	};

} // namespace DuiLib

#endif //__UITEXT_H__