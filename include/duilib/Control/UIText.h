#ifndef __UITEXT_H__
#define __UITEXT_H__

#pragma once

namespace DuiLib
{
	class UILIB_API CTextUI : public CLabelUI
	{
	public:
		CTextUI();
		~CTextUI();

		LPCTSTR GetClass() const;
		UINT GetControlFlags() const;
		LPVOID GetInterface(LPCTSTR pstrName);

		virtual void DoEvent(TEventUI& event);
		SIZE EstimateSize(SIZE szAvailable);        
	protected:
	};

} // namespace DuiLib

#endif //__UITEXT_H__