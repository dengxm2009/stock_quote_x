#pragma once
//////////////BEGIN消息映射宏定义////////////////////////////////////////////////////
///

namespace DuiLib
{

enum DuiSig
{
	DuiSig_end = 0, // [marks end of message map]
	DuiSig_lwl,     // LRESULT (WPARAM, LPARAM)
	DuiSig_vn,      // void (TNotifyUI)
};

class CControlUI;

// Structure for notifications to the outside world
typedef struct tagTNotifyUI 
{
	DWORD sType;
	CDuiString sVirtualWnd;
	CControlUI* pSender;
	DWORD dwTimestamp;
	POINT ptMouse;
	WPARAM wParam;
	LPARAM lParam;
} TNotifyUI;

class CNotifyPump;
typedef void (CNotifyPump::*DUI_PMSG)(TNotifyUI& msg);  //指针类型

union DuiMessageMapFunctions
{
	DUI_PMSG pfn;   // generic member function pointer
	LRESULT (CNotifyPump::*pfn_Notify_lwl)(WPARAM, LPARAM);
	void (CNotifyPump::*pfn_Notify_vn)(TNotifyUI&);
};

//定义所有消息类型
//////////////////////////////////////////////////////////////////////////

#define DUI_MSGTYPE_MENU                   0x001    //(_T("menu"))
#define DUI_MSGTYPE_LINK                   0x001    //(_T("link"))

#define DUI_MSGTYPE_TIMER                  0x005    //(_T("timer"))
#define DUI_MSGTYPE_CLICK                  0x006    //(_T("click"))

#define DUI_MSGTYPE_RETURN                 0x00A    //(_T("return"))
#define DUI_MSGTYPE_SCROLL                 0x00B    //(_T("scroll"))

#define DUI_MSGTYPE_DROPUP                 0x020    //(_T("dropup"))
#define DUI_MSGTYPE_DROPDOWN               0x021    //(_T("dropdown"))
#define DUI_MSGTYPE_SETFOCUS               0x022    //(_T("setfocus"))

#define DUI_MSGTYPE_KILLFOCUS              0x030    //(_T("killfocus"))
#define DUI_MSGTYPE_ITEMCLICK 		   	   0x031    //(_T("itemclick"))
#define DUI_MSGTYPE_ITEMRCLICK             0x032    //(_T("itemrclick"))
#define DUI_MSGTYPE_TABSELECT              0x033    //(_T("tabselect"))

#define DUI_MSGTYPE_ITEMSELECT 		   	   0x040    //(_T("itemselect"))
#define DUI_MSGTYPE_ITEMEXPAND             0x041    //(_T("itemexpand"))
#define DUI_MSGTYPE_WINDOWINIT             0x042    //(_T("windowinit"))
#define DUI_MSGTYPE_BUTTONDOWN 		   	   0x043    //(_T("buttondown"))
#define DUI_MSGTYPE_BUTTONUP 		   	   0x044    //(_T("buttonup"))
#define DUI_MSGTYPE_MOUSEENTER			   0x045    //(_T("mouseenter"))
#define DUI_MSGTYPE_MOUSELEAVE			   0x046    //(_T("mouseleave"))

#define DUI_MSGTYPE_TEXTCHANGED            0x050    //(_T("textchanged"))
#define DUI_MSGTYPE_HEADERCLICK            0x051    //(_T("headerclick"))
#define DUI_MSGTYPE_ITEMDBCLICK            0x052    //(_T("itemdbclick"))
#define DUI_MSGTYPE_SHOWACTIVEX            0x053    //(_T("showactivex"))

#define DUI_MSGTYPE_ITEMCOLLAPSE           0x060    //(_T("itemcollapse"))
#define DUI_MSGTYPE_ITEMACTIVATE           0x061    //(_T("itemactivate"))
#define DUI_MSGTYPE_VALUECHANGED           0x062    //(_T("valuechanged"))

#define DUI_MSGTYPE_SELECTCHANGED 		   0x070    //(_T("selectchanged"))

#define DUI_MSGTYPE_CLICKMENU              0x079    //(_T("clickmenu"))
#define DUI_MSGTYPE_CHECKMENU              0x07A    //(_T("checkmenu"))

#define DUI_MSGTYPE_USERMESSAGE			   0xffff0000	// 用户自定义事件开始


//////////////////////////////////////////////////////////////////////////



struct DUI_MSGMAP_ENTRY;
struct DUI_MSGMAP
{
#ifndef UILIB_STATIC
	const DUI_MSGMAP* (PASCAL* pfnGetBaseMap)();
#else
	const DUI_MSGMAP* pBaseMap;
#endif
	const DUI_MSGMAP_ENTRY* lpEntries;
};

//结构定义
struct DUI_MSGMAP_ENTRY //定义一个结构体，来存放消息信息
{
	CDuiString sMsgType;          // DUI消息类型
	CDuiString sCtrlName;         // 控件名称
	UINT       nSig;              // 标记函数指针类型
	DUI_PMSG   pfn;               // 指向函数的指针
};

#define BACKCOLOR_TRANSPARENT   (0xFFFF00FF)

//定义
#ifndef UILIB_STATIC
#define DUI_DECLARE_MESSAGE_MAP()                                         \
private:                                                                  \
	static const DUI_MSGMAP_ENTRY _messageEntries[];                      \
protected:                                                                \
	static const DUI_MSGMAP messageMap;                                   \
	static const DUI_MSGMAP* PASCAL _GetBaseMessageMap();                 \
	virtual const DUI_MSGMAP* GetMessageMap() const;                      \

#else
#define DUI_DECLARE_MESSAGE_MAP()                                         \
private:                                                                  \
	static const DUI_MSGMAP_ENTRY _messageEntries[];                      \
protected:                                                                \
	static  const DUI_MSGMAP messageMap;				                  \
	virtual const DUI_MSGMAP* GetMessageMap() const;                      \

#endif


//基类声明开始
#ifndef UILIB_STATIC
#define DUI_BASE_BEGIN_MESSAGE_MAP(theClass)                              \
	const DUI_MSGMAP* PASCAL theClass::_GetBaseMessageMap()               \
		{ return NULL; }                                                  \
	const DUI_MSGMAP* theClass::GetMessageMap() const                     \
		{ return &theClass::messageMap; }                                 \
	UILIB_COMDAT const DUI_MSGMAP theClass::messageMap =                  \
		{  &theClass::_GetBaseMessageMap, &theClass::_messageEntries[0] };\
	UILIB_COMDAT const DUI_MSGMAP_ENTRY theClass::_messageEntries[] =     \
	{                                                                     \

#else
#define DUI_BASE_BEGIN_MESSAGE_MAP(theClass)                              \
	const DUI_MSGMAP* theClass::GetMessageMap() const                     \
		{ return &theClass::messageMap; }                                 \
	UILIB_COMDAT const DUI_MSGMAP theClass::messageMap =                  \
		{  NULL, &theClass::_messageEntries[0] };                         \
	UILIB_COMDAT const DUI_MSGMAP_ENTRY theClass::_messageEntries[] =     \
	{                                                                     \

#endif


//子类声明开始
#ifndef UILIB_STATIC
#define DUI_BEGIN_MESSAGE_MAP(theClass, baseClass)                        \
	const DUI_MSGMAP* PASCAL theClass::_GetBaseMessageMap()               \
		{ return &baseClass::messageMap; }                                \
	const DUI_MSGMAP* theClass::GetMessageMap() const                     \
		{ return &theClass::messageMap; }                                 \
	UILIB_COMDAT const DUI_MSGMAP theClass::messageMap =                  \
		{ &theClass::_GetBaseMessageMap, &theClass::_messageEntries[0] }; \
	UILIB_COMDAT const DUI_MSGMAP_ENTRY theClass::_messageEntries[] =     \
	{                                                                     \

#else
#define DUI_BEGIN_MESSAGE_MAP(theClass, baseClass)                        \
	const DUI_MSGMAP* theClass::GetMessageMap() const                     \
		{ return &theClass::messageMap; }                                 \
	UILIB_COMDAT const DUI_MSGMAP theClass::messageMap =                  \
		{ &baseClass::messageMap, &theClass::_messageEntries[0] };        \
	UILIB_COMDAT const DUI_MSGMAP_ENTRY theClass::_messageEntries[] =     \
	{                                                                     \

#endif


//声明结束
#define DUI_END_MESSAGE_MAP()                                             \
	{ _T(""), _T(""), DuiSig_end, (DUI_PMSG)0 }                           \
};                                                                        \


//定义消息类型--执行函数宏
#define DUI_ON_MSGTYPE(msgtype, memberFxn)                                \
	{ msgtype, _T(""), DuiSig_vn, (DUI_PMSG)&memberFxn},                  \


//定义消息类型--控件名称--执行函数宏
#define DUI_ON_MSGTYPE_CTRNAME(msgtype,ctrname,memberFxn)                 \
	{ msgtype, ctrname, DuiSig_vn, (DUI_PMSG)&memberFxn },                \


//定义click消息的控件名称--执行函数宏
#define DUI_ON_CLICK_CTRNAME(ctrname,memberFxn)                           \
	{ DUI_MSGTYPE_CLICK, ctrname, DuiSig_vn, (DUI_PMSG)&memberFxn },      \


//定义selectchanged消息的控件名称--执行函数宏
#define DUI_ON_SELECTCHANGED_CTRNAME(ctrname,memberFxn)                   \
    { DUI_MSGTYPE_SELECTCHANGED,ctrname,DuiSig_vn,(DUI_PMSG)&memberFxn }, \


//定义killfocus消息的控件名称--执行函数宏
#define DUI_ON_KILLFOCUS_CTRNAME(ctrname,memberFxn)                       \
	{ DUI_MSGTYPE_KILLFOCUS,ctrname,DuiSig_vn,(DUI_PMSG)&memberFxn },     \


//定义menu消息的控件名称--执行函数宏
#define DUI_ON_MENU_CTRNAME(ctrname,memberFxn)                            \
	{ DUI_MSGTYPE_MENU,ctrname,DuiSig_vn,(DUI_PMSG)&memberFxn },          \


//定义与控件名称无关的消息宏

  //定义timer消息--执行函数宏
#define DUI_ON_TIMER()                                                    \
	{ DUI_MSGTYPE_TIMER, _T(""), DuiSig_vn,(DUI_PMSG)&OnTimer },          \


///
//////////////END消息映射宏定义////////////////////////////////////////////////////


//////////////BEGIN控件名称宏定义//////////////////////////////////////////////////
///MenuListLabel

#define  DUI_CTR_EDIT                            _T("Edit")
#define  DUI_CTR_LIST                            _T("List")
#define  DUI_CTR_TEXT                            _T("Text")
#define  DUI_CTR_MENU                            _T("Menu")

#define  DUI_CTR_COMBO                           _T("Combo")
#define  DUI_CTR_LABEL                           _T("Label")

#define  DUI_CTR_BUTTON                          _T("Button")
#define  DUI_CTR_OPTION                          _T("Option")
#define  DUI_CTR_SLIDER                          _T("Slider")

#define  DUI_CTR_CONTROL                         _T("Control")
#define  DUI_CTR_ACTIVEX                         _T("ActiveX")

#define  DUI_CTR_LISTITEM                        _T("ListItem")
#define  DUI_CTR_PROGRESS                        _T("Progress")
#define  DUI_CTR_RICHEDIT                        _T("RichEdit")
#define  DUI_CTR_CHECKBOX                        _T("CheckBox")
#define  DUI_CTR_COMBOBOX                        _T("ComboBox")
#define  DUI_CTR_DATETIME                        _T("DateTime")
#define  DUI_CTR_TREEVIEW                        _T("TreeView")
#define  DUI_CTR_TREENODE                        _T("TreeNode")
#define  DUI_CTR_MENUITEM                        _T("MenuItem")

#define  DUI_CTR_CONTAINER                       _T("Container")
#define  DUI_CTR_TABLAYOUT                       _T("TabLayout")
#define  DUI_CTR_SCROLLBAR                       _T("ScrollBar")

#define  DUI_CTR_LISTHEADER                      _T("ListHeader")
#define  DUI_CTR_TILELAYOUT                      _T("TileLayout")
#define  DUI_CTR_WEBBROWSER                      _T("WebBrowser")

#define  DUI_CTR_CHILDLAYOUT                     _T("ChildLayout")
#define  DUI_CTR_LISTELEMENT                     _T("ListElement")
#define  DUI_CTR_IMAGENUMBER                     _T("ImageNumber")

#define  DUI_CTR_DIALOGLAYOUT                    _T("DialogLayout")

#define  DUI_CTR_VERTICALLAYOUT                  _T("VerticalLayout")
#define  DUI_CTR_LISTHEADERITEM                  _T("ListHeaderItem")

#define  DUI_CTR_LISTTEXTELEMENT                 _T("ListTextElement")

#define  DUI_CTR_HORIZONTALLAYOUT                _T("HorizontalLayout")
#define  DUI_CTR_LISTLABELELEMENT                _T("ListLabelElement")
#define  DUI_CTR_MENULABELELEMENT                _T("MenuLabelElement")
#define  DUI_CTR_MENULABELELEMENT                _T("MenuLabelElement")

#define  DUI_CTR_LISTCONTAINERELEMENT            _T("ListContainerElement")

#define DUI_CLASS_NAME(ctrlname)                 (ctrlname _T("UI"))

// 内部使用的
#define DUI_CTR_ILIST                           _T("IList")
#define DUI_CTR_ILISTOWNER                      _T("IListOwner")
#define DUI_CTR_ICONTAINER                      _T("IContainer")

///
//////////////END控件名称宏定义//////////////////////////////////////////////////
}// namespace DuiLib

