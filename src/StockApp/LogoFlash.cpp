#include "stdafx.h"
#include "LogoFlash.h"
#include "FABase/thread.h"
#include "common.h"
#include "UIManager.h"

CLogoUI::CLogoUI()
:m_pDlgUI(0)
{
}


CLogoUI::~CLogoUI()
{
}


LRESULT CLogoUI::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	if (FAILED(__super::OnCreate(uMsg, wParam, lParam, bHandled)))
		return S_FALSE;

	return S_OK;
}

LRESULT CLogoUI::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	__super::OnClose(uMsg, wParam, lParam, bHandled);
	return S_OK;
}

//消息提醒
void CLogoUI::Notify(TNotifyUI &  msg)
{
	if (msg.sType == DUI_MSGTYPE_CLICK){
		if (msg.pSender->GetName() == _T("BtnClose")){
			Close(0);
		}
	}
	return;
}

void * CLogoUI::SwitchUI(const bool *isstop, void * param)
{

	CLogoUI * p = (CLogoUI*)param;
	if (!p)return 0;

	Sleep(TIME_SWITCH_LOGIN_UI);
	
	if (p->m_pDlgUI)
		p->m_pDlgUI->Close();

	if (!UI->GetLoginUI()->ShowLogon(nullptr)){
		return FALSE;
	}


	return 0;
}


UINT CLogoUI::ShowLogon(HWND h)
{
	//设置变量
	ASSERT(NULL == m_pDlgUI);

	CLogoUI dl;
	m_pDlgUI = &dl;
	if (dl.Create(h) == NULL)
		return S_FALSE;
	m_pDlgUI->CenterWindow();

	//定时切换
	CYKAutoThread th(new CYKThread(SwitchUI, this));
	th->start();

	UINT nret = m_pDlgUI->ShowModal();
	m_pDlgUI = NULL;
	return nret;
}