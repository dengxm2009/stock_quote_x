#include "stdafx.h"
#include "LoginUI.h"
#include "UIManager.h"

CLoginUI::CLoginUI()
:m_pDlgUI(0)
{
}


CLoginUI::~CLoginUI()
{
}


LRESULT CLoginUI::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	if (FAILED(__super::OnCreate(uMsg, wParam, lParam, bHandled)))
		return S_FALSE;

	return S_OK;
}

LRESULT CLoginUI::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	__super::OnClose(uMsg, wParam, lParam, bHandled);
	return S_OK;
}

//消息提醒
void CLoginUI::Notify(TNotifyUI &  msg)
{
	if (msg.sType == DUI_MSGTYPE_CLICK){
		if (msg.pSender->GetName() == _T("BtnClose")){
			Close(0);
		}
		else if (msg.pSender->GetName() == _T("BtnLogin")){
			
		}
		else if (msg.pSender->GetName() == _T("BtnVistor")){
			UI->GetLoginUI()->OnLoginOK();
		}
	}
	return;
}

void CLoginUI::OnLoginOK()
{
	if (m_pDlgUI)
		m_pDlgUI->Close();
}


UINT CLoginUI::ShowLogon(HWND h)
{
	//设置变量
	ASSERT(NULL == m_pDlgUI);

	CLoginUI dl;
	m_pDlgUI = &dl;
	if (dl.Create(h) == NULL)
		return S_FALSE;
	m_pDlgUI->CenterWindow();
	UINT nret = m_pDlgUI->ShowModal();
	m_pDlgUI = NULL;
	if (nret == 0)
	{
		exit(0);
	}
	return nret;
}