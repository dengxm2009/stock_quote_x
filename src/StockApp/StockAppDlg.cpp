
// StockAppDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "StockApp.h"
#include "StockAppDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CStockAppUI::CStockAppUI()
:m_pDlgUI(0)
{
}

CStockAppUI::~CStockAppUI()
{
	return;
}


LRESULT CStockAppUI::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	if (FAILED(__super::OnCreate(uMsg, wParam, lParam, bHandled)))
		return S_FALSE;

	return S_OK;
}

LRESULT CStockAppUI::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled){
	__super::OnClose(uMsg, wParam, lParam, bHandled);
	return S_OK;
}

//消息提醒
void CStockAppUI::Notify(TNotifyUI &  msg)
{
	if (msg.sType == DUI_MSGTYPE_CLICK){
		if (msg.pSender->GetName() == _T("BtnClose")){
			Close(0);
		}
	}
	return;
}

void CStockAppUI::OnUpdate(tagSinaTick * tick)
{
	CLabelUI * pTxtName = (CLabelUI *)m_PaintManager.FindControl(_T("txtName"));
	if (pTxtName) pTxtName->SetText(tick->name);

	CLabelUI * pTxtPrice= (CLabelUI *)m_PaintManager.FindControl(_T("txtPrice"));
	char szT[33];
	sprintf(szT, "%.2f", tick->fNowPrice);
	if (pTxtPrice) pTxtPrice->SetText(szT);

}