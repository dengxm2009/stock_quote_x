#pragma once

class CLoginUI : public WindowImplBase
{
	//函数定义
public:
	//构造函数
	CLoginUI();
	//析构函数
	virtual ~CLoginUI();
	// 构造
public:
	virtual CDuiString GetSkinFolder()      { return _T("../stock"); }
	virtual CDuiString GetSkinFile()        { return _T("login.xml"); }
	virtual LPCTSTR    GetWndTitle()const   { return _T("登录"); }
	virtual SIZE       GetWndSize()const    { SIZE s = { CW_USEDEFAULT, CW_USEDEFAULT }; return s; }
	virtual UINT       GetWndStyle()const   { return UI_WNDSTYLE_DLG_NOSIZE; }
	virtual UINT       GetWndExStyle()const { return UI_WNDSTYLE_EX_FRAME; }
	virtual LPCTSTR    GetWindowClassName() const { return _T("QSS_LOGIN"); }

	//重载函数
public:
	//消息提醒
	virtual void Notify(TNotifyUI &  msg);
	virtual LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	virtual LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


public:
	UINT ShowLogon(HWND h);

	void OnLoginOK();

private:

	CLoginUI    *                  m_pDlgUI;                        //登录窗口
};
