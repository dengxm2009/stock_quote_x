#pragma once

#define DEFAULT_CODE "sh601015"

#define TIME_SWITCH_LOGIN_UI	3000

typedef vector<string> vString;
typedef vString::iterator vStringIter;

typedef size_t(*SinaCallBack)(char *ptr, size_t size, size_t nmemb, string *stream);

//一档价量
struct tagFB
{
	int     volume;
	float   fPrice;
};

//sqlite分档
enum enIndex
{
	NAME = 0,
	OPEN,
	CLOSE,
	PRICE,
	HIGH,
	LOW,
	ASK1,
	BID1,
	VOLUME,
	AMT,
	DAY = AMT + 21,
	TIME,
	OTHER,
};

#define STR_LEN     33
#define FB_NUMS     5

#define PREFIX_SZ_STOCK	"sz"	//深市代码前缀
#define PREFIX_SH_STOCK	"sh"	//沪市

#define STOCK_SINA_URL			("http://hq.sinajs.cn/list=")

struct tagSinaTick
{
	char		szCode[STR_LEN];	  //代码
	char		name[STR_LEN];        //股票名称
	float		fOpen;                //今日开盘价
	float		fLastClose;           //昨日收盘价
	float		fNowPrice;            //当前价格
	float		fHigh;                //今日最高
	float		fLow;                 //今日最低
	float		fJHAsk1;              //集合竞价卖1价
	float		fJHBid1;              //集合竞价买1价
	int			nVolume;              //成交量（含*100）
	float		fAMT;                 //成交量
	tagFB		fb[2][FB_NUMS];       //5档行情
	char		date[STR_LEN];        //日期
	char		time[STR_LEN];        //时间
	char		offset[STR_LEN];      //保留字符串
	int			timestamp;			  //时间戳

};