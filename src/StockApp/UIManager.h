#pragma once
#include "common.h"
#include "LoginUI.h"
#include "LogoFlash.h"
#include "StockAppDlg.h"
#include "FABase/thread.h"
#include "FABase/single.h"

class CUIManager
{
public:
	CUIManager();
	~CUIManager();
public:
	void Sub();
	static void* _SubFunc(const bool *isstop, void *param);
	int PullQuote(char *szCode, SinaCallBack pCall);
private:
	CYKThread *m_th;
public:
	static CLoginUI * GetLoginUI() { static CLoginUI ml; return &ml; }

	static CLogoUI * GetLogoUI() { static CLogoUI ml; return &ml; }
	
	static CStockAppUI * GetAppUI() { static CStockAppUI s; return &s; }
};

#define UI singleton_t<CUIManager>::instance()