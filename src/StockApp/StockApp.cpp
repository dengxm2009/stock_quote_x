
// StockApp.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "StockApp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


const TCHAR szPlazaClass[] = TEXT("stock");				//广场类名
const TCHAR szProductKey[] = TEXT("stockKey");           //产品主键

// CStockAppApp

CStockAppUI *m_pDlgUI;

BEGIN_MESSAGE_MAP(CStockAppApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CStockAppApp 构造

CStockAppApp::CStockAppApp()
{
	// 支持重新启动管理器
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO:  在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CStockAppApp 对象

CStockAppApp theApp;


// CStockAppApp 初始化

BOOL CStockAppApp::InitInstance()
{

	__super::InitInstance();

	//环境配置
	AfxOleInit();
	AfxInitRichEdit2();
	InitCommonControls();
	AfxEnableControlContainer();

	SetRegistryKey(szProductKey);

	//变量定义
	WNDCLASS WndClasss;
	ZeroMemory(&WndClasss, sizeof(WndClasss));
	//注册窗口
	WndClasss.style = CS_DBLCLKS;
	WndClasss.hIcon = LoadIcon(IDR_MAINFRAME);
	WndClasss.lpfnWndProc = DefWindowProc;
	WndClasss.lpszClassName = szPlazaClass;
	WndClasss.hInstance = AfxGetInstanceHandle();
	WndClasss.hCursor = LoadStandardCursor(MAKEINTRESOURCE(IDC_ARROW));
	if (AfxRegisterClass(&WndClasss) == FALSE) AfxThrowResourceException();

	if (NULL == UI->GetAppUI()->Create()){
		return FALSE;
	}
	UI->GetAppUI()->ShowWindow(false);

	if (!UI->GetLogoUI()->ShowLogon(nullptr)){
		return FALSE;
	}

	m_pMainWnd = CWnd::FromHandle(UI->GetAppUI()->GetHWND());

	UI->GetAppUI()->CenterWindow();
	BringWindowToTop(UI->GetAppUI()->GetHWND());
	UI->Sub();
	UI->GetAppUI()->ShowModal();
	
	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

