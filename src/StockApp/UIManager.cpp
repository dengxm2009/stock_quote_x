#include "stdafx.h"
#include "UIManager.h"
#include "FABase/StringUtil.h"
#include "curl/curl.h"

int Localtime_To_Unixtime(int year, int month, int day, int hour, int minute, int second)
{
	int monthtable[13] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
	int  unixtimetemp = 0, unixtimeday = 0, unixtimehour = 0;
	char i = 0;
	int x = 0, extraday = 0;
	int  unixtimeyear = 0;
	char  unixtimemonth = 0;

	for (x = 1970; x < year; x++)               /*加上闰年二月多的一天*/
	{
		if (((x % 4) == 0 && ((x % 100) != 0 || (x % 400) == 0)))
		{
			extraday++;
		}
	}

	unixtimeyear = year - 1970;
	if ((((year) % 4) == 0 && (((year) % 100) != 0 || ((year) % 400) == 0)))
	{
		for (i = 2; i < 12; i++)
		{
			monthtable[i] = monthtable[i] + 1;
		}
	}
	unixtimemonth = month;
	unixtimehour = hour;
	unixtimeday = unixtimeyear * 365 + monthtable[unixtimemonth - 1] + day - 1 + extraday;
	/*修改UnixHour-8可能导致变成负数的问题，原来为“(UnixHour-8)*3600”*/
	/*ZG20080126 修改UnixHour*3600可能导致数据溢出的问题，现在改为(unixtimehour)*3600*/
	unixtimetemp = unixtimeday * 86400 + (unixtimehour)* 3600 - 28800 + minute * 60 + second;
	return(unixtimetemp);
}

int get_timestamp(char *strIn)
{
	int nYear;
	int nMonth;
	int nDate;
	int nHour;
	int nMin;
	int nSec;
	char *sFormat = ("%d-%d-%d %d:%d:%d");
	sscanf_s(strIn, sFormat, &nYear, &nMonth, &nDate, &nHour, &nMin, &nSec);
	return Localtime_To_Unixtime(nYear, nMonth, nDate, nHour, nMin, nSec);
}


void GB2312ToUtf8(const char *file, char *szBufOut, int nOutSize)
{
	WCHAR   *pwchar = 0;
	int len = 0;
	int codepage = AreFileApisANSI() ? CP_ACP : CP_OEMCP;
	len = MultiByteToWideChar(codepage, 0, file, -1, NULL, 0);
	pwchar = new WCHAR[len];
	if (pwchar != 0)
	{
		len = MultiByteToWideChar(codepage, 0, file, -1, pwchar, len);
		if (len != 0)
		{
			len = WideCharToMultiByte(CP_UTF8, 0, pwchar, -1, szBufOut, nOutSize, 0, 0);
		}
		delete pwchar;
	}
}


void Split(char *sz, int len, vString & v, string &code)
{
	string full("");
	full.append(sz, len - 3);  //删除末尾2个字符：";
	std::size_t found = full.find("=");
	if (found == full.npos)
	{
		return;
	}

	//code
	code = full.substr(13, 6);

	full = full.substr(found + 2, len - 3 - found - 1);
	//boost::split(v, full, boost::is_any_of(","));

	CStringUtil::Split(v, full, ",");
}


int SinaDataParser(char *sz, int len, tagSinaTick*pTick)
{

	/*
	var hq_str_sh601006="

	大秦铁路,
	9.47,
	9.56,
	9.39,
	9.55,
	9.30,
	9.39,
	9.40,
	25150964,
	237040260,

	38600,9.39,
	41708,9.38,

	88700,9.37,
	54300,9.36,
	29916,9.35,
	22200,9.40,

	9100,9.41,
	36100,9.42,
	31500,9.43,
	75200,9.44,

	2015-09-10,13:28:27,00";

	struct tagFB
	{
	int     volume;
	float   fPrice;
	};


	struct tagStock
	{
	TCHAR name[STR_LEN];        //股票名称
	float fOpen;                //今日开盘价
	float fLastClose;           //昨日收盘价
	float fNowPrice;            //当前价格
	float fHigh;                //今日最高
	float fLow;                 //今日最低
	float fJHAsk1;              //集合竞价卖1价
	float fJHBid1;              //集合竞价买1价
	int   nVolume;              //成交量（含*100）
	float fAMT;                 //成交量
	TCHAR date[STR_LEN];        //日期
	TCHAR time[STR_LEN];        //时间
	tagFB fb[FB_NUMS];
	TCHAR offset[STRL_LEN];     //保留字符串

	};
	*/

	vString v;
	v.clear();
	string code("");
	Split(sz, len, v, code);
	if (v.size() != OTHER + 1)
	{
		return false;
	}

	for (vStringIter it = v.begin(); it != v.end(); ++it)
	{

	}


	//GB2312ToUtf8(v[NAME].c_str(), pTick->name, STR_LEN);
	strcpy(pTick->name, v[NAME].c_str());

	pTick->fOpen = atof(v[OPEN].c_str());
	pTick->fLastClose = atof(v[CLOSE].c_str());
	pTick->fNowPrice = atof(v[PRICE].c_str());
	pTick->fHigh = atof(v[HIGH].c_str());
	pTick->fLow = atof(v[LOW].c_str());
	pTick->fJHAsk1 = atof(v[ASK1].c_str());
	pTick->fJHBid1 = atof(v[BID1].c_str());

	pTick->nVolume = atoi(v[VOLUME].c_str());
	pTick->fAMT = atof(v[AMT].c_str());


	for (int j = 0; j < FB_NUMS; j++)
	{
		pTick->fb[0][j].volume = atoi(v[AMT + j * 2 + 1].c_str());
		pTick->fb[0][j].fPrice = atof(v[AMT + j * 2 + 2].c_str());

		pTick->fb[0][j].volume /= 100;
	}

	for (int j = 0; j < FB_NUMS; j++)
	{
		pTick->fb[1][j].volume = atoi(v[AMT + 10 + j * 2 + 1].c_str());
		pTick->fb[1][j].fPrice = atof(v[AMT + 10 + j * 2 + 2].c_str());

		pTick->fb[1][j].volume /= 100;
	}

	sprintf_s(pTick->date, STR_LEN, "%s", v[DAY].c_str());
	sprintf_s(pTick->time, STR_LEN, "%s", v[TIME].c_str());
	sprintf_s(pTick->szCode, STR_LEN, "%s", code.c_str());

	//日期转时间戳
	char szTime[128];
	sprintf_s(szTime, 128, "%s %s", pTick->date, pTick->time);
	pTick->timestamp = get_timestamp(szTime);

	return true;
}



static size_t sina_callback(char *ptr, size_t size, size_t nmemb, string *stream)
{
	int len = size * nmemb;
	int written = len;
	stream->append(ptr, len);
	tagSinaTick tick;
	memset(&tick, 0, sizeof(tick));
	if (SinaDataParser(ptr, len, &tick))
	{
		printf("[Quote]%s,%.1f\n", tick.szCode, tick.fNowPrice);

		UI->GetAppUI()->OnUpdate(&tick);
	}
	return written;
}

///////////////////////////////////////////////////////////////////////////////////////////////

CUIManager::CUIManager()
:m_th(0)
{
}


CUIManager::~CUIManager()
{
}


int CUIManager::PullQuote(char *szCode, SinaCallBack pCall)
{
	//string m_page;
	CURL *curl;
	CURLcode res;
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();
	if (curl)
	{
		// 设置调试模式  
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
		char code[MAX_PATH] = { 0 };
		sprintf_s(code, 256, "%s%s", STOCK_SINA_URL, szCode);
		// 设置回调函数  
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, pCall);

		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20);        //设置超时
		curl_easy_setopt(curl, CURLOPT_URL, code);//URL
		//curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 0);
		//curl_easy_setopt(curl, CURLOPT_WRITEDATA, &m_page);
		//curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);        //屏蔽其它信号
		curl_easy_setopt(curl, CURLOPT_HEADER, 0);          //下载数据包括HTTP头部
		//m_page.clear();

		res = curl_easy_perform(curl);

		/* always cleanup */
		curl_easy_cleanup(curl);
	}

	return 0;
}


void* CUIManager::_SubFunc(const bool *isstop, void *param)
{
	CUIManager * p = (CUIManager*)param;
	if (!p) return 0;
	while (1)
	{
		p->PullQuote(DEFAULT_CODE, sina_callback);
		Sleep(500);
	}
	return 0;
}

void CUIManager::Sub()
{
	m_th = new CYKThread(_SubFunc, this);
	m_th->start();
}