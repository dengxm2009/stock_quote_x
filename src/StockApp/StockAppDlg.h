
// StockAppDlg.h : 头文件
//

#pragma once
#include "LoginUI.h"
#include "LogoFlash.h"

// CStockAppUI 对话框
class CStockAppUI : public WindowImplBase
{
	//函数定义
public:
	//构造函数
	CStockAppUI();
	//析构函数
	virtual ~CStockAppUI();
// 构造
public:
	virtual CDuiString GetSkinFolder()      { return _T("../stock"); }
	virtual CDuiString GetSkinFile()        { return _T("quote.xml"); }
	virtual LPCTSTR    GetWndTitle()const   { return _T("登录"); }
	virtual SIZE       GetWndSize()const    { SIZE s = { CW_USEDEFAULT, CW_USEDEFAULT }; return s; }
	virtual UINT       GetWndStyle()const   { return UI_WNDSTYLE_DLG_NOSIZE; }
	virtual UINT       GetWndExStyle()const { return UI_WNDSTYLE_EX_FRAME; }
	virtual LPCTSTR    GetWindowClassName() const { return _T("QSS_APP"); }

	//重载函数
public:
	//消息提醒
	virtual void Notify(TNotifyUI &  msg);
	virtual LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	virtual LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

public:
	void OnUpdate(tagSinaTick * tick);

private:
	CStockAppUI	* m_pDlgUI;
};