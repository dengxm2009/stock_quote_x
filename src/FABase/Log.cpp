#include "FABase/Log.h"

#ifdef WIN32
#include "windows.h"
#include <stdio.h>
#endif

bool  HasFile(const char *name)
{
	HANDLE	file;
	WIN32_FIND_DATAA	filedata;

	file=FindFirstFileA(name,&filedata);

	if(file == INVALID_HANDLE_VALUE)
		return false;
	else
	{
		FindClose(file);
		return true;
	}

	return false;
}

void _addfilename(char *filename,int size)
{
	int i=size-1,j=0;

	while(filename[i] != '.')
		i--;

	i--;

	while(true)
	{
		if(filename[i-j] == '9')
			filename[i-j]='0';
		else
		{
			filename[i-j]++;

			break;
		}

		j++;
	}
}

void  _format_str(char *buf,char *text,...)
{
	va_list ap;

	va_start(ap,text);
	vsprintf_s(buf,1024,text,ap);
	va_end(ap);
}

void  OutputLog(char *text,...)
{
	static	DWORD	g_dwLogRecordNum	=	0;
	static	char	g_LogFileName[256]	=	"run.log";

	const	int	MAX_LOG_RECORD_NUM	=	89139;

	if( g_dwLogRecordNum < MAX_LOG_RECORD_NUM )								//最多只写10M的日志
	{
		char buf[1024];
		FILE *fp=NULL;

		va_list ap;

		va_start(ap,text);
		vsprintf_s(buf,1024,text,ap);
		va_end(ap);	

		if( g_dwLogRecordNum == 0 )
		{
			char strModuleName[256];
			int iLen = GetModuleFileNameA(NULL, strModuleName, 255);			//GetCurrentProcess(),
			int iPos = 0;
			for (int i=iLen-1; i>=0; i--)
			{
				if(strModuleName[i] == '.')
					strModuleName[i] = 0;									//作为字符串的新结尾

				if(strModuleName[i] == '\\')
				{	
					iPos = i+1;
					break;
				}
			}

			if(iLen > 0)
				sprintf_s(g_LogFileName,256,"%s_000.log",strModuleName+iPos);

			while(HasFile(g_LogFileName))
			{
				_addfilename(g_LogFileName,sizeof(g_LogFileName));
			}
			fopen_s(&fp,g_LogFileName,"w");

		}
		else
		{
			fopen_s(&fp,g_LogFileName,"a");
		}

		if(fp == NULL)
			return ;

		g_dwLogRecordNum++;

		SYSTEMTIME time;
		GetLocalTime(&time);
		//加一个线程ID的输出 add by fxh 2006.08.09
		fprintf(fp, "%d: %d: %s - %d/%d/%d %d:%d:%d \n", g_dwLogRecordNum,GetCurrentThreadId(), buf, time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);

		if( g_dwLogRecordNum > MAX_LOG_RECORD_NUM - 2 )
		{
			g_dwLogRecordNum++;
			fprintf( fp,"%d: %d: 日志文件大于%d行，新日志将不再记录！\n",g_dwLogRecordNum,GetCurrentThreadId(),MAX_LOG_RECORD_NUM - 2 );   
		}

		fclose(fp);
	}
	else
	{
		//if( !HasFile(g_LogFileName) )									//如果原日志被删除，则日志行数重新开始计数
		{
			g_dwLogRecordNum = 0;
		}
	}
}

void  Log(char *text,...)
{
	char buf[1024];

	va_list ap;

	va_start(ap,text);
	vsprintf_s(buf,1024,text,ap);
	va_end(ap);	

	//output to file
	OutputLog(buf);
}