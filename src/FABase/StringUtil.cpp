#include "FABase/StringUtil.h"

void CStringUtil::Split(VStr&vs,const string&inString,const string&Separator)
{
	size_t start, pos;
	start = 0;
	do 
	{
		pos = inString.find_first_of(Separator, start);
		if (pos == start)
		{
			start = pos + 1;
		}
		else if (pos == string::npos)
		{
			vs.push_back(inString.substr(start).c_str());
			break;
		}
		else
		{
			vs.push_back(inString.substr(start, pos - start).c_str());
			start = pos + 1;
		}
		start = inString.find_first_not_of(Separator, start);

	} while (pos != string::npos);
}

void CStringUtil::SplitW(VWStr&vs,const wstring&inString,const wstring&Separator)
{
	size_t start, pos;
	start = 0;
	do 
	{
		pos = inString.find_first_of(Separator, start);
		if (pos == start)
		{
			start = pos + 1;
		}
		else if (pos == wstring::npos)
		{
			vs.push_back(inString.substr(start).c_str());
			break;
		}
		else
		{
			vs.push_back(inString.substr(start, pos - start).c_str());
			start = pos + 1;
		}
		start = inString.find_first_not_of(Separator, start);

	} while (pos != string::npos);
}
