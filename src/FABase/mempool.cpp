#include "FABase/mempool.h"

CMemPool * CMemPool::Instance()
{
	static CMemPool pool;
	return &pool;
}


int32_t CMemPool::Adjust(int32_t chunk_size)
{
	if (chunk_size < 1)
		return 0;
	else if (chunk_size < CHUNK_128)
		return CHUNK_128;
	else if (chunk_size < CHUNK_256)
		return CHUNK_256;
	else if (chunk_size < CHUNK_512)
		return CHUNK_512;
	else if (chunk_size < CHUNK_1024)
		return CHUNK_1024;
	else if (chunk_size < CHUNK_2048)
		return CHUNK_2048;
	else if (chunk_size < CHUNK_MAX)
		return CHUNK_MAX;
	else
		return chunk_size;
}

CMemPool::CMemPool()
{
	mBlocks.insert(std::make_pair(CHUNK_128, VChunk()));
	mBlocks.insert(std::make_pair(CHUNK_256, VChunk()));
	mBlocks.insert(std::make_pair(CHUNK_512, VChunk()));
	mBlocks.insert(std::make_pair(CHUNK_1024, VChunk()));
	mBlocks.insert(std::make_pair(CHUNK_2048, VChunk()));
	mBlocks.insert(std::make_pair(CHUNK_MAX, VChunk()));
}

CMemPool::~CMemPool()
{
	Clear(CHUNK_128);
	Clear(CHUNK_256);
	Clear(CHUNK_512);
	Clear(CHUNK_1024);
	Clear(CHUNK_2048);
	Clear(CHUNK_MAX);
	mBlocks.clear();
}

void CMemPool::Clear(int32_t chunk_size)
{
	std::vector<LPChunk>& v = mBlocks[chunk_size];
	std::vector<LPChunk>::iterator itr;

	for (itr = v.begin(); itr != v.end(); ++itr) {
		void* buf = (char*)*itr - 4;
		free(buf);
	}
	v.clear();
}

LPChunk	CMemPool::AllocChunk(int32_t size)
{
	LPChunk ret = NULL;
	size = Adjust(size);

	//大于X的内存，直接申请，不进入vector队列
	//其他内存，从vector队尾获取
	if (size <= CHUNK_MAX) {
		CYKAutoMutex guard(&mMutex);
		std::vector<void*>& v = mBlocks[size];
		if (v.size() > 0) {
			ret = v.back();
			v.pop_back();
		}
	}

	if (NULL == ret) {
		ret = malloc(size + 4);
		if (NULL != ret) {
			int32_t* pi = (int32_t*)ret;
			*pi = size;						//前4字节保存空间大小
			ret = (char*)ret + 4;			//实际数据存储开始
		}
	}
	return ret;
}

void	CMemPool::FreeChunk(LPPChunk chunk)
{
	if (!chunk || !(*chunk)) {
		return;
	}

	int32_t* pi = (int32_t*)((char*)(*chunk) - 4);
	if (*pi > CHUNK_MAX) {
		free(pi);
	}
	else {
		CYKAutoMutex guard(&mMutex);
		mBlocks[*pi].push_back(*chunk);
	}

	*chunk = NULL;
}
