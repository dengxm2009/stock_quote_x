#include "FABase/AutoPt.h"
#include "FABase/mempool.h"

void CYKMemCounter::free()
{
	CYKMemCounter * p = this;
	MEMPOOL->FreeChunk((LPPChunk)&p);
}